# Go Chi PostgreSQL REST API Example

> Go, PostgreSQL, REST API Blog example. Authentication by Authorization Bearer jwt token Header ot cookies jwt token. Role based routes protection. Create, update, delete and Get data with pagination and sorting query params.

## Usage

Rename ".env.example" to ".env" and update the values/settings to your own



## API Documentation

Documentation with examples [link](https://documenter.getpostman.com/view/4199988/SW17Ra47?version=latest)

## Used
* [Chi](https://github.com/go-chi/chi)
* [sqlx](https://github.com/jmoiron/sqlx)
* [pgx](https://github.com/jackc/pgx)
* [jwt-go](https://github.com/dgrijalva/jwt-go)
* [bcryptjs](https://godoc.org/golang.org/x/crypto/bcrypt) 
* [validator](https://github.com/go-playground/validator)
* [postgresql](https://www.postgresql.org/)
* [godotenv](https://github.com/joho/godotenv)



##### Author [Alexander Bryksin](https://github.com/AleksK1NG)