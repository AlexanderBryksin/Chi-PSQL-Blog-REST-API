package main

import (
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/server"
	"github.com/joho/godotenv"
	"log"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	server.Run()
}

// TODO:
// 1. Create GitHub repo
// 2. Create Folders
// 3. Use Routes folder
