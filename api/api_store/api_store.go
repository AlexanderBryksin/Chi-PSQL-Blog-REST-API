package api_store

import (
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/repositories"
	"github.com/jackc/pgx/v4"
)

type ApiStore struct {
	userRepo    repositories.UserRepo
	postRepo    repositories.PostRepo
	commentRepo repositories.CommentRepo
	db          *pgx.Conn
}

// Create new store
func NewStore(conn *pgx.Conn) *ApiStore {
	userRepo := repositories.NewUserRepository(conn)
	postRepo := repositories.NewPostRepository(conn)
	commentRepo := repositories.NewCommentRepository(conn)

	return &ApiStore{
		userRepo:    userRepo,
		postRepo:    postRepo,
		commentRepo: commentRepo,
		db:          conn,
	}
}

// API Store Interface
type Store interface {
	User()
	Post()
	Comment()
}

// Api store User repo
func (s *ApiStore) User() repositories.UserRepo {
	return s.userRepo
}

// Api store Post repo
func (s *ApiStore) Post() repositories.PostRepo {
	return s.postRepo
}

// Api store Comment repo
func (s *ApiStore) Comment() repositories.CommentRepo {
	return s.commentRepo
}
