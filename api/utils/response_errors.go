package utils

import "github.com/pkg/errors"

var (
	ErrNoResult                     = errors.New("no result")
	ErrUserWithEmailAlreadyExist    = errors.New("user with email already exist")
	ErrUserWithUsernameAlreadyExist = errors.New("user with username already exist")
	ErrInvalidEmailOrPassword       = errors.New("invalid email or password")
	ErrInvalidUserId                = errors.New("user with given id not found")
	ErrBadRequest                   = errors.New("bad request")
	ErrInvalidToken                 = errors.New("invalid token")
	ErrUnauthorized                 = errors.New("unauthorized")
	ErrRoleAccess                   = errors.New("unauthorized user role for this route")
)
