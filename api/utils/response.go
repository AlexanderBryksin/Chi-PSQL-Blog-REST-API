package utils

import (
	"encoding/json"
	"net/http"
)

// JSON Response with data and status code
func JsonResponse(w http.ResponseWriter, data interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(statusCode)

	if data == nil {
		data = map[string]string{}
	}

	if err := json.NewEncoder(w).Encode(data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	return
}

// Bad Request Response
func BadRequestResponse(w http.ResponseWriter, err error) {
	response := map[string]string{"error": err.Error()}
	JsonResponse(w, response, http.StatusBadRequest)
}
