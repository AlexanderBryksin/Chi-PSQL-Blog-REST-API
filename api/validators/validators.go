package validators

import (
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/pkg/errors"
	"gopkg.in/go-playground/validator.v9"
)

// Validate user struct fields
func ValidateRegisterUser(user *models.User) error {
	validate := validator.New()

	if err := validate.Struct(user); err != nil {
		return err
	}

	return nil
}

func ValidateLoginUser(user *models.User) error {
	validate := validator.New()

	if err := validate.Var(user.Email, "required,email"); err != nil {
		return errors.New("invalid user email or password")
	}

	if err := validate.Var(user.Password, "required,required,gte=6,lte=250"); err != nil {
		return errors.New("invalid user email or password")
	}

	return nil
}

// Validate post struct fields
func ValidatePost(post *models.Post) error {
	validate := validator.New()

	if err := validate.Struct(post); err != nil {
		return err
	}

	return nil
}

// Validate comment struct fields
func ValidateComment(comment *models.Comment) error {
	validate := validator.New()

	if err := validate.Struct(comment); err != nil {
		return err
	}

	return nil
}

func ValidateCommentMessage(comment models.Comment) error {
	validate := validator.New()

	if err := validate.Var(comment.Message, "required,gte=0,lte=5000"); err != nil {
		return err
	}

	return nil
}
