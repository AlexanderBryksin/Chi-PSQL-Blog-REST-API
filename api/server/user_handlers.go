package server

import (
	"encoding/json"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/jwt_util"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/validators"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

// Register new user in database
func (s *Server) RegisterUser() http.HandlerFunc {
	type Response struct {
		User  *models.User `json:"user"`
		Token string       `json:"token"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User

		err := json.NewDecoder(r.Body).Decode(&user)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		if err = validators.ValidateRegisterUser(&user); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		newUser, err := s.store.User().RegisterUser(user)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		token, err := jwt_util.GenerateJWT(&user)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		tokenCookie := http.Cookie{
			Name:     "jwt-token",
			Value:    token,
			Path:     "/",
			MaxAge:   60 * 60 * 24,
			Secure:   false,
			HttpOnly: true,
		}

		newUser.Sanitize()
		userWithToken := &Response{
			User:  newUser,
			Token: token,
		}

		http.SetCookie(w, &tokenCookie)
		utils.JsonResponse(w, userWithToken, http.StatusCreated)
	}
}

// Login user by email and password
func (s *Server) LoginUser() http.HandlerFunc {
	type Response struct {
		User  *models.User `json:"user"`
		Token string       `json:"token"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User

		err := json.NewDecoder(r.Body).Decode(&user)
		if err != nil {
			utils.BadRequestResponse(w, err)
		}

		if err = validators.ValidateLoginUser(&user); err != nil {
			utils.BadRequestResponse(w, utils.ErrInvalidEmailOrPassword)
			return
		}

		foundUser, err := s.store.User().LoginUser(user)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		if err = foundUser.VerifyUserPassword(user.Password); err != nil {
			utils.BadRequestResponse(w, utils.ErrInvalidEmailOrPassword)
			return
		}

		token, err := jwt_util.GenerateJWT(foundUser)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		tokenCookie := http.Cookie{
			Name:     "jwt-token",
			Value:    token,
			Path:     "/",
			MaxAge:   60 * 60 * 24,
			Secure:   false,
			HttpOnly: true,
		}

		foundUser.Sanitize()
		userWithToken := &Response{
			User:  foundUser,
			Token: token,
		}

		http.SetCookie(w, &tokenCookie)
		utils.JsonResponse(w, userWithToken, http.StatusCreated)
	}
}

// Load user by id
func (s *Server) GetUserById() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userId := chi.URLParam(r, "userId")
		if userId == "" {
			utils.BadRequestResponse(w, utils.ErrInvalidEmailOrPassword)
			return
		}

		id, err := strconv.Atoi(userId)
		if err != nil {
			utils.BadRequestResponse(w, utils.ErrInvalidUserId)
			return
		}

		user, err := s.store.User().GetUserByID(id)
		if err != nil {
			utils.BadRequestResponse(w, utils.ErrInvalidUserId)
			return
		}

		user.Sanitize()
		utils.JsonResponse(w, user, http.StatusOK)
	}
}

// Load user from auth context
func (s *Server) LoadUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value("user").(*models.User)
		if !ok {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		user.Sanitize()
		utils.JsonResponse(w, user, http.StatusOK)
	}
}

// Update user in database, required id query param
func (s *Server) UpdateUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User

		userId := chi.URLParam(r, "userId")
		if userId == "" {
			utils.BadRequestResponse(w, utils.ErrInvalidEmailOrPassword)
			return
		}

		err := json.NewDecoder(r.Body).Decode(&user)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		id, err := strconv.ParseInt(userId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		user.ID = id

		updatedUser, err := s.store.User().UpdateUser(user)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		user.Sanitize()
		utils.JsonResponse(w, updatedUser, http.StatusOK)
	}
}

// Delete user by id query param
func (s *Server) DeleteUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userId := chi.URLParam(r, "userId")
		if userId == "" {
			utils.BadRequestResponse(w, utils.ErrInvalidEmailOrPassword)
			return
		}

		deletedUser, err := s.store.User().DeleteUser(userId)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, deletedUser, http.StatusOK)
	}
}

// Get all users
func (s *Server) GetAllUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()

		limit := query.Get("limit")
		offset := query.Get("offset")
		orderBy := query.Get("orderby")

		queryParams := map[string]string{
			"limit":   limit,
			"offset":  offset,
			"orderby": orderBy,
		}

		allUsers, err := s.store.User().GetAllUsers(queryParams)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, allUsers, http.StatusOK)
	}
}
