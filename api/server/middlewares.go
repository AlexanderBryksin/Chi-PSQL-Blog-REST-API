package server

import (
	"context"
	"fmt"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"os"
	"strconv"
	"strings"
)

// Load user middleware, decode Authorization Bearer header token, fin user in database and pass it to context
func (s *Server) LoadUserMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		bearerHeader := r.Header.Get("Authorization")

		if bearerHeader != "" {
			headerParts := strings.Split(bearerHeader, " ")
			if len(headerParts) == 2 {
				tokenString := headerParts[1]

				s.ValidateJWTToken(w, r, tokenString, next)
				return
			}
		} else {

			tokenCookie, err := r.Cookie("jwt-token")
			if err != nil || tokenCookie.Value == "" {
				next.ServeHTTP(w, r)
				return
			}

			s.ValidateJWTToken(w, r, tokenCookie.Value, next)
			return
		}
	}

	return http.HandlerFunc(fn)
}

// Group authentication protection
func (s *Server) EnforceAuthenticatedMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value("user").(*models.User)
		if ok && user != nil {
			ctx := context.WithValue(r.Context(), "user", user)
			next.ServeHTTP(w, r.WithContext(ctx))
			return
		} else {
			utils.BadRequestResponse(w, utils.ErrUnauthorized)
			return
		}
	}

	return http.HandlerFunc(fn)
}

// Auth protected route for authorized users
func (s *Server) AuthProtectedRoute(next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value("user").(*models.User)
		if ok && user != nil {
			ctx := context.WithValue(r.Context(), "user", user)
			next.ServeHTTP(w, r.WithContext(ctx))
			return
		} else {
			utils.BadRequestResponse(w, utils.ErrUnauthorized)
			return
		}
	}
}

// Validate jwt token, decode user id, find in database and pass by context user struct
func (s *Server) ValidateJWTToken(w http.ResponseWriter, r *http.Request, tokenString string, next http.Handler) {
	// Parse, validate, and return a token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (tkn interface{}, err error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signin method %v", token.Header["alg"])
		}
		secret := []byte(os.Getenv("JWT_SECRET_KEY"))
		return secret, nil
	})
	if err != nil {
		next.ServeHTTP(w, r)
		return
	}

	if !token.Valid {
		next.ServeHTTP(w, r)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		fmt.Println(claims)
		userId, ok := claims["userId"].(string)

		fmt.Printf("[+] Authenticated request, authenticated user id is %d\n", userId)
		if !ok {
			next.ServeHTTP(w, r)
			return
		}

		idInt, err := strconv.Atoi(userId)
		if err != nil {
			next.ServeHTTP(w, r)
			return
		}

		user, err := s.store.User().GetUserByID(idInt)
		if err != nil {
			next.ServeHTTP(w, r)
			return
		}

		ctx := context.WithValue(r.Context(), "user", user)

		next.ServeHTTP(w, r.WithContext(ctx))
	}
}

// Role based authorization middleware
func (s *Server) RoleAuthMiddleware(next http.Handler, roles ...string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value("user").(*models.User)
		if ok && user != nil && user.Role != "" {
			for _, role := range roles {
				if role == user.Role {
					ctx := context.WithValue(r.Context(), "user", user)
					next.ServeHTTP(w, r.WithContext(ctx))
					return
				} else {
					utils.BadRequestResponse(w, utils.ErrRoleAccess)
					return
				}
			}
		} else {
			utils.BadRequestResponse(w, utils.ErrRoleAccess)
			return
		}
	}
}
