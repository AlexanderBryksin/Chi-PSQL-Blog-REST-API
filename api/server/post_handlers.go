package server

import (
	"encoding/json"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/validators"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

// Create post in database
func (s *Server) CreatePost() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var post models.Post
		user, ok := r.Context().Value("user").(*models.User)
		if !ok {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		err := json.NewDecoder(r.Body).Decode(&post)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		post.AuthorId = user.ID

		if err = validators.ValidatePost(&post); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		createdPost, err := s.store.Post().CreatePost(post)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, createdPost, http.StatusCreated)
	}
}

// UpdatePost in database
func (s *Server) UpdatePost() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var post models.Post

		postId := chi.URLParam(r, "postId")
		if postId == "" {
			utils.BadRequestResponse(w, utils.ErrInvalidEmailOrPassword)
			return
		}

		user, ok := r.Context().Value("user").(*models.User)
		if !ok {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		id, err := strconv.ParseInt(postId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		if err = s.CheckUserIsPostAuthor(r, id); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&post)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		post.ID = id
		post.AuthorId = user.ID

		if err = validators.ValidatePost(&post); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		updatedPost, err := s.store.Post().UpdatePost(post)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, updatedPost, http.StatusOK)
	}
}

// Delete post in database
func (s *Server) DeletePost() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		postId := chi.URLParam(r, "postId")

		if postId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		id, err := strconv.ParseInt(postId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		if err = s.CheckUserIsPostAuthor(r, id); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		result, err := s.store.Post().DeletePost(id)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, result, http.StatusOK)
	}
}

// Get post id query param from database
func (s *Server) GetPostById() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		postId := chi.URLParam(r, "postId")

		if postId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		id, err := strconv.ParseInt(postId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		post, err := s.store.Post().GetPostByID(id)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, post, http.StatusOK)
	}
}

// Get all posts with pagination and sorting
func (s *Server) GetAllPosts() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()

		limit := query.Get("limit")
		offset := query.Get("offset")
		orderBy := query.Get("orderby")

		queryParams := map[string]string{
			"limit":   limit,
			"offset":  offset,
			"orderby": orderBy,
		}

		posts, err := s.store.Post().GetAllPosts(queryParams)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, posts, http.StatusOK)
	}
}

// Check current user is post author helper
func (s *Server) CheckUserIsPostAuthor(r *http.Request, postId int64) error {
	user, ok := r.Context().Value("user").(*models.User)
	if !ok {
		return utils.ErrUnauthorized
	}

	post, err := s.store.Post().GetPostByID(postId)
	if err != nil {
		return utils.ErrBadRequest
	}

	if post.AuthorId != user.ID {
		return utils.ErrUnauthorized
	}

	return nil
}
