package server

import (
	"context"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/api_store"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/db"
	"github.com/go-chi/chi"
	"log"
	"net/http"
	"os"
	"time"
)

type Server struct {
	router *chi.Mux
	store  *api_store.ApiStore
}

func NewServer(router *chi.Mux, str *api_store.ApiStore) *Server {
	return &Server{router: router, store: str}
}

func Run() {
	log.Println("Starting GO Server ...")

	// init psql database instance
	conn := db.NewDB()
	defer conn.Close(context.Background())

	// get port from .env file
	port := os.Getenv("PORT")

	// Init Chi touter
	r := chi.NewRouter()

	// Init api api_store
	apiStore := api_store.NewStore(conn)

	// Create server instance
	s := NewServer(r, apiStore)

	// Init routes
	s.InitRoutes()

	// Init Server
	server := &http.Server{
		Addr:           port,
		Handler:        s.router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(server.ListenAndServe())
}
