package server

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"time"
)

// Init Router
func (s *Server) InitRoutes() {

	s.router.Use(middleware.RequestID)
	s.router.Use(middleware.RealIP)
	s.router.Use(middleware.Logger)
	s.router.Use(middleware.Compress(6, "application/json"))
	s.router.Use(middleware.Recoverer)
	s.router.Use(middleware.URLFormat)
	s.router.Use(middleware.Timeout(60 * time.Second))

	s.router.Route("/api/v1", func(r chi.Router) {
		r.Route("/auth", func(r chi.Router) {
			r.Group(func(r chi.Router) {
				r.Get("/", s.GetAllUsers())
				r.Post("/register", s.RegisterUser())
				r.Post("/login", s.LoginUser())
				r.Get("/{userId}", s.GetUserById())
			})

			r.Group(func(r chi.Router) {
				r.Use(s.LoadUserMiddleware)
				r.Use(s.EnforceAuthenticatedMiddleware)

				r.Get("/me", s.LoadUser())

				r.Put("/{userId}", s.RoleAuthMiddleware(s.UpdateUser(), "admin"))
				r.Delete("/{userId}", s.RoleAuthMiddleware(s.DeleteUser(), "admin"))
			})
		})

		r.Route("/posts", func(r chi.Router) {
			r.Group(func(r chi.Router) {
				r.Get("/{postId}", s.GetPostById())
				r.Get("/", s.GetAllPosts())
			})

			r.Group(func(r chi.Router) {
				r.Use(s.LoadUserMiddleware)
				r.Use(s.EnforceAuthenticatedMiddleware)

				r.Post("/", s.CreatePost())
				r.Put("/{postId}", s.UpdatePost())
				r.Delete("/{postId}", s.DeletePost())

			})
		})

		r.Route("/posts/{postId}/comments", func(r chi.Router) {
			r.Group(func(r chi.Router) {
				r.Get("/", s.GetAllComments())
			})

			r.Group(func(r chi.Router) {
				r.Use(s.LoadUserMiddleware)
				r.Use(s.EnforceAuthenticatedMiddleware)

				r.Post("/", s.CreateComment())
				r.Put("/{commentId}", s.UpdateComment())
			})
		})

		r.Route("/comments", func(r chi.Router) {
			r.Group(func(r chi.Router) {
				r.Get("/{commentId}", s.GetCommentById())
			})

			r.Group(func(r chi.Router) {
				r.Use(s.LoadUserMiddleware)
				r.Use(s.EnforceAuthenticatedMiddleware)

				r.Delete("/{commentId}", s.DeleteComment())
			})
		})

	})

}
