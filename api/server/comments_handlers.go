package server

import (
	"encoding/json"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/validators"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

// Create comment in database
func (s *Server) CreateComment() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var comment models.Comment

		err := json.NewDecoder(r.Body).Decode(&comment)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		postId := chi.URLParam(r, "postId")
		if postId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		pId, err := strconv.ParseInt(postId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		comment.PostId = pId

		user, ok := r.Context().Value("user").(*models.User)
		if !ok {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		comment.AuthorId = user.ID

		if err = validators.ValidateComment(&comment); err != nil {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		createdComment, err := s.store.Comment().CreateComment(comment)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, createdComment, http.StatusCreated)
	}
}

// Update comment in database
func (s *Server) UpdateComment() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value("user").(*models.User)
		if !ok {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		var comment models.Comment

		commentId := chi.URLParam(r, "commentId")
		if commentId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		cId, err := strconv.ParseInt(commentId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		if err := s.CheckUserIsCommentAuthor(r, cId); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&comment)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		postId := chi.URLParam(r, "postId")
		if postId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		pId, err := strconv.ParseInt(postId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		comment.ID = cId
		comment.PostId = pId
		comment.AuthorId = user.ID

		if err = validators.ValidateComment(&comment); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		updatedComment, err := s.store.Comment().UpdateComment(comment)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, updatedComment, http.StatusOK)
	}
}

// Delete comment from database
func (s *Server) DeleteComment() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		commentId := chi.URLParam(r, "commentId")
		if commentId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		id, err := strconv.ParseInt(commentId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		if err = s.CheckUserIsCommentAuthor(r, id); err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		result, err := s.store.Comment().DeleteComment(id)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, result, http.StatusOK)
	}
}

// Delete comment from database
func (s *Server) GetCommentById() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		commentId := chi.URLParam(r, "commentId")
		if commentId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		id, err := strconv.ParseInt(commentId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		comment, err := s.store.Comment().GetCommentByID(id)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, comment, http.StatusOK)
	}
}

// Get all posts with pagination and sorting
func (s *Server) GetAllComments() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		postId := chi.URLParam(r, "postId")

		if postId == "" {
			utils.BadRequestResponse(w, utils.ErrBadRequest)
			return
		}

		id, err := strconv.ParseInt(postId, 10, 64)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		query := r.URL.Query()

		limit := query.Get("limit")
		offset := query.Get("offset")
		orderBy := query.Get("orderby")

		queryParams := map[string]string{
			"limit":   limit,
			"offset":  offset,
			"orderby": orderBy,
		}

		comments, err := s.store.Comment().GetAllComments(queryParams, id)
		if err != nil {
			utils.BadRequestResponse(w, err)
			return
		}

		utils.JsonResponse(w, comments, http.StatusOK)
	}
}

// Check current user is comment author helper
func (s *Server) CheckUserIsCommentAuthor(r *http.Request, commentId int64) error {
	user, ok := r.Context().Value("user").(*models.User)
	if !ok {
		return utils.ErrUnauthorized
	}

	comment, err := s.store.Comment().GetCommentByID(commentId)
	if err != nil {
		return utils.ErrBadRequest
	}

	if comment.AuthorId != user.ID {
		return utils.ErrUnauthorized
	}

	return nil
}
