package models

import (
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"time"
)

type User struct {
	ID       int64  `json:"id,omitempty"`
	Username string `json:"username,omitempty" validate:"required"`
	Email    string `json:"email,omitempty" validate:"required,email"`
	Password string `json:"password,omitempty" validate:"required,gte=6,lte=250"`
	Role     string `json:"role,omitempty"`

	CreatedAt time.Time `json:"createdAt,omitempty"`
	UpdatedAt time.Time `json:"updatedAt,omitempty"`
}

// Clean user password
func (u *User) Sanitize() {
	u.Password = ""
}

func (u *User) SetTimestamps() {
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

// Hash user password
func (u *User) HashUserPassword() {
	hashPassword, err := utils.HashPassword(u.Password)
	if err != nil {
		return
	}
	u.Password = string(hashPassword)
}

// Verify compare user hashed password (from db) and password string from request
func (u *User) VerifyUserPassword(password string) error {
	return utils.VerifyPassword(u.Password, password)
}
