package models

import "time"

type Comment struct {
	ID       int64  `json:"id,omitempty"`
	Message  string `json:"message,omitempty" validate:"required,gte=0,lte=5000"`
	AuthorId int64  `json:"author_id,omitempty" validate:"required"`
	PostId   int64  `json:"post_id,omitempty" validate:"required"`

	CreatedAt time.Time `json:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
}

func (c *Comment) SetTimestamps() {
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
}
