package models

import "time"

type Post struct {
	ID          int64  `json:"id,omitempty"`
	Title       string `json:"title,omitempty" validate:"required,gte=0,lte=250"`
	Description string `json:"description,omitempty" validate:"required,gte=0,lte=5000"`
	AuthorId    int64  `json:"author_id,omitempty" validate:"required"`
	Category    string `json:"category,omitempty" validate:"required"`

	CreatedAt time.Time `json:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
}

func (p *Post) SetTimestamps() {
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()
}
