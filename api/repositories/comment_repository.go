package repositories

import (
	"context"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"github.com/jackc/pgx/v4"
	"time"
)

// User repository struct
type CommentRepository struct {
	db *pgx.Conn
}

// Create new comments repository
func NewCommentRepository(db *pgx.Conn) *CommentRepository {
	return &CommentRepository{db: db}
}

// Comment repository interface
type CommentRepo interface {
	CreateComment(comment models.Comment) (*models.Comment, error)
	UpdateComment(comment models.Comment) (*models.Comment, error)
	DeleteComment(commentId int64) (interface{}, error)
	GetCommentByID(commentId int64) (*models.Comment, error)
	GetAllComments(queryParams map[string]string, postId int64) ([]*models.Comment, error)
}

func (c CommentRepository) CreateComment(comment models.Comment) (*models.Comment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	comment.SetTimestamps()

	sqlQuery := `INSERT INTO comments (message, author_id, post_id, created_at, updated_at) VALUES ($1, $2, $3, $4, $5) RETURNING id`

	if err := c.db.QueryRow(ctx, sqlQuery, &comment.Message, &comment.AuthorId, &comment.PostId, &comment.CreatedAt, &comment.UpdatedAt).Scan(&comment.ID); err != nil {
		return nil, utils.ErrBadRequest
	}

	return &comment, nil
}

func (c CommentRepository) UpdateComment(comment models.Comment) (*models.Comment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	comment.UpdatedAt = time.Now()

	sqlQuery := `UPDATE comments SET message = $1, updated_at = $2 WHERE id = $3`

	_, err := c.db.Exec(ctx, sqlQuery, &comment.Message, &comment.UpdatedAt, &comment.ID)
	if err != nil {
		return nil, utils.ErrBadRequest
	}

	return &comment, nil
}

func (c CommentRepository) DeleteComment(commentId int64) (interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	sqlQuery := `DELETE FROM comments WHERE id = $1`

	exec, err := c.db.Exec(ctx, sqlQuery, commentId)
	if err != nil {
		return nil, utils.ErrBadRequest
	}

	rowsAffected := exec.RowsAffected()

	return map[string]interface{}{"rows deleted": rowsAffected}, nil
}

func (c CommentRepository) GetCommentByID(commentId int64) (*models.Comment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findQuery := `SELECT id, message, author_id, post_id, updated_at, created_at FROM comments WHERE id = $1`

	var comment models.Comment
	if err := c.db.QueryRow(ctx, findQuery, commentId).Scan(&comment.ID, &comment.Message, &comment.AuthorId, &comment.PostId, &comment.UpdatedAt, &comment.CreatedAt); err != nil {
		return nil, utils.ErrBadRequest
	}

	return &comment, nil
}

func (c CommentRepository) GetAllComments(queryParams map[string]string, postId int64) ([]*models.Comment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var comments []*models.Comment

	limit := "10"
	offset := "0"
	orderBy := "id"

	queryLimit, ok := queryParams["limit"]
	if ok && queryLimit != "" {
		limit = queryLimit
	}

	queryOffset, ok := queryParams["offset"]
	if ok && queryOffset != "" {
		offset = queryOffset
	}

	queryOrderBy, ok := queryParams["orderby"]
	if ok && queryOrderBy != "" {
		orderBy = queryOrderBy
	}

	sqlQuery := `SELECT id, message, author_id, post_id, updated_at, created_at FROM comments WHERE post_id = $1 ORDER BY $2 LIMIT $3 OFFSET $4`

	rows, err := c.db.Query(ctx, sqlQuery, postId, orderBy, limit, offset)
	if err != nil {
		return nil, utils.ErrNoResult
	}
	defer rows.Close()

	for rows.Next() {
		var comment models.Comment
		if err := rows.Scan(&comment.ID, &comment.Message, &comment.AuthorId, &comment.PostId, &comment.UpdatedAt, &comment.CreatedAt); err != nil {
			return nil, utils.ErrNoResult
		}
		comments = append(comments, &comment)
	}

	return comments, nil
}
