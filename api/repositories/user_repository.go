package repositories

import (
	"context"
	"errors"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"github.com/jackc/pgx/v4"
	"strconv"
	"time"
)

// User repository struct
type UserRepository struct {
	db *pgx.Conn
}

// User repository interface
type UserRepo interface {
	RegisterUser(user models.User) (*models.User, error)
	LoginUser(user models.User) (*models.User, error)
	GetUserByID(userId int) (*models.User, error)
	UpdateUser(user models.User) (*models.User, error)
	DeleteUser(userId string) (interface{}, error)
	GetAllUsers(map[string]string) ([]*models.User, error)
}

// Create new user repository instance
func NewUserRepository(db *pgx.Conn) *UserRepository {
	return &UserRepository{db: db}
}

// Register user in database
func (u *UserRepository) RegisterUser(user models.User) (*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findQuery := `SELECT email, username FROM users WHERE email = $1`

	var checkUser models.User
	err := u.db.QueryRow(ctx, findQuery, &user.Email).Scan(&checkUser.Email, &checkUser.Username)
	if err == nil || checkUser.Email != "" {
		return nil, errors.New("user with given email already exists")
	}

	user.SetTimestamps()
	user.HashUserPassword()

	sqlQuery := `INSERT INTO users (username, password, email, created_at, updated_at, role) VALUES($1, $2, $3, $4, $5, $6) RETURNING id`

	err = u.db.QueryRow(ctx, sqlQuery, &user.Username, &user.Password, &user.Email, &user.CreatedAt, &user.UpdatedAt, &user.Role).Scan(&user.ID)
	if err != nil {
		return nil, err
	}

	user.Sanitize()

	return &user, nil
}

// Login user, return user by email
func (u *UserRepository) LoginUser(user models.User) (*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findQuery := `select email, password, username, role, id FROM users WHERE email = $1`

	var foundUser models.User
	if err := u.db.QueryRow(ctx, findQuery, user.Email).Scan(&foundUser.Email, &foundUser.Password, &foundUser.Username, &foundUser.Role, &foundUser.ID); err != nil {
		return nil, err
	}

	return &foundUser, nil
}

// Get user by id
func (u *UserRepository) GetUserByID(userId int) (*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findQuery := `SELECT id, password, username, email, role FROM users WHERE id = $1`

	var user models.User
	if err := u.db.QueryRow(ctx, findQuery, userId).Scan(&user.ID, &user.Password, &user.Username, &user.Email, &user.Role); err != nil {
		return nil, utils.ErrBadRequest
	}

	return &user, nil
}

// Update user
func (u *UserRepository) UpdateUser(user models.User) (*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	sqlQuery := `UPDATE users SET username = $1, email = $2, updated_at = $3, role = $4 WHERE id = $5`

	user.UpdatedAt = time.Now()
	_, err := u.db.Exec(ctx, sqlQuery, &user.Username, &user.Email, &user.UpdatedAt, &user.Role, &user.ID)
	if err != nil {
		return nil, utils.ErrBadRequest
	}

	return &user, nil
}

// Delete user
func (u *UserRepository) DeleteUser(userId string) (interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	id, err := strconv.Atoi(userId)
	if err != nil {
		return nil, utils.ErrInvalidUserId
	}

	sqlQuery := `DELETE FROM users WHERE id = $1`

	_, err = u.db.Exec(ctx, sqlQuery, id)
	if err != nil {
		return nil, utils.ErrBadRequest
	}

	return map[string]string{"message": "User deleted"}, nil
}

// Get all users
func (u *UserRepository) GetAllUsers(queryParams map[string]string) ([]*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var users []*models.User

	limit := "10"
	offset := "0"
	orderBy := "id"

	queryLimit, ok := queryParams["limit"]
	if ok && queryLimit != "" {
		limit = queryLimit
	}

	queryOffset, ok := queryParams["offset"]
	if ok && queryOffset != "" {
		offset = queryOffset
	}

	queryOrderBy, ok := queryParams["orderby"]
	if ok && queryOrderBy != "" {
		orderBy = queryOrderBy
	}

	sqlQuery := `SELECT id, username, email, role FROM users ORDER BY $1 LIMIT $2 OFFSET $3`

	rows, err := u.db.Query(ctx, sqlQuery, orderBy, limit, offset)
	if err != nil {
		return nil, utils.ErrNoResult
	}
	defer rows.Close()

	for rows.Next() {
		var user models.User
		if err := rows.Scan(&user.ID, &user.Username, &user.Email, &user.Role); err != nil {
			return nil, utils.ErrNoResult
		}
		users = append(users, &user)
	}

	return users, nil
}
