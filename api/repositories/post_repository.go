package repositories

import (
	"context"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/models"
	"github.com/aleksk1ng/chi-psql-blog-rest-api/api/utils"
	"github.com/jackc/pgx/v4"
	"time"
)

// User repository struct
type PostRepository struct {
	db *pgx.Conn
}

// Crate new Post repo instance
func NewPostRepository(db *pgx.Conn) *PostRepository {
	return &PostRepository{db: db}
}

// Post repository interface
type PostRepo interface {
	CreatePost(post models.Post) (*models.Post, error)
	UpdatePost(post models.Post) (*models.Post, error)
	DeletePost(postId int64) (interface{}, error)
	GetPostByID(postId int64) (*models.Post, error)
	GetAllPosts(queryParams map[string]string) ([]*models.Post, error)
}

// Create post
func (p PostRepository) CreatePost(post models.Post) (*models.Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	post.SetTimestamps()

	sqlQuery := `INSERT INTO posts (title, description, author_id, category, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	if err := p.db.QueryRow(ctx, sqlQuery, &post.Title, &post.Description, &post.AuthorId, &post.Category, &post.CreatedAt, &post.UpdatedAt).Scan(&post.ID); err != nil {
		return nil, utils.ErrBadRequest
	}

	return &post, nil
}

// Update post
func (p PostRepository) UpdatePost(post models.Post) (*models.Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	post.UpdatedAt = time.Now()

	sqlQuery := `UPDATE posts SET title = $1, description = $2, category = $3, updated_at = $4 WHERE id = $5`

	_, err := p.db.Exec(ctx, sqlQuery, &post.Title, &post.Description, &post.Category, &post.UpdatedAt, &post.ID)
	if err != nil {
		return nil, utils.ErrBadRequest
	}

	return &post, nil
}

// Delete post
func (p PostRepository) DeletePost(postId int64) (interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	sqlQuery := `DELETE FROM posts WHERE id = $1`

	exec, err := p.db.Exec(ctx, sqlQuery, postId)
	if err != nil {
		return nil, utils.ErrBadRequest
	}

	rowsAffected := exec.RowsAffected()

	return map[string]interface{}{"rows deleted": rowsAffected}, nil

}

// Get post by id
func (p PostRepository) GetPostByID(postId int64) (*models.Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findQuery := `SELECT id, title, description, author_id, category, updated_at, created_at FROM posts WHERE id = $1`

	var post models.Post
	if err := p.db.QueryRow(ctx, findQuery, postId).Scan(&post.ID, &post.Title, &post.Description, &post.AuthorId, &post.Category, &post.UpdatedAt, &post.CreatedAt); err != nil {
		return nil, utils.ErrBadRequest
	}

	return &post, nil

}

func (p PostRepository) GetAllPosts(queryParams map[string]string) ([]*models.Post, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var posts []*models.Post

	limit := "10"
	offset := "0"
	orderBy := "id"

	queryLimit, ok := queryParams["limit"]
	if ok && queryLimit != "" {
		limit = queryLimit
	}

	queryOffset, ok := queryParams["offset"]
	if ok && queryOffset != "" {
		offset = queryOffset
	}

	queryOrderBy, ok := queryParams["orderby"]
	if ok && queryOrderBy != "" {
		orderBy = queryOrderBy
	}

	sqlQuery := `SELECT id, title, description, author_id, category, updated_at, created_at FROM posts ORDER BY $1 LIMIT $2 OFFSET $3`

	rows, err := p.db.Query(ctx, sqlQuery, orderBy, limit, offset)
	if err != nil {
		return nil, utils.ErrNoResult
	}
	defer rows.Close()

	for rows.Next() {
		var post models.Post
		if err := rows.Scan(&post.ID, &post.Title, &post.Description, &post.AuthorId, &post.Category, &post.UpdatedAt, &post.CreatedAt); err != nil {
			return nil, utils.ErrNoResult
		}
		posts = append(posts, &post)
	}

	return posts, nil
}
