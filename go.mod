module github.com/aleksk1ng/chi-psql-blog-rest-api

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/jackc/pgx v3.6.0+incompatible // indirect
	github.com/jackc/pgx/v4 v4.1.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/pkg/errors v0.8.1
	golang.org/x/crypto v0.0.0-20191029031824-8986dd9e96cf
	golang.org/x/tools v0.0.0-20191030232956-1e24073be82c // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0
)
